# Pasta Library
A collection of classes which make developing games across platforms easier. The
pasta library aims to provide an interface for common tasks such as:

  * Sharing
  * Licensing
  * Settings
  * Persistence

