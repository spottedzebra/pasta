﻿using System.Diagnostics;
using Pasta.ViewModels;

namespace Pasta.Win7.ViewModels
{
    public class Win7UrlLauncher : IUrlLauncher
    {
        public void LaunchUrl(string url)
        {
            Process.Start(url);
        }
    }
}