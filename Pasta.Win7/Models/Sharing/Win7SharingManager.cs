﻿using System;
using System.Diagnostics;
using System.Web;
using Pasta.Models.Sharing;

namespace Pasta.Win7.Models.Sharing
{
    public class Win7SharingManager : ISharingManager
    {
        private SharingState sharingState;
        private ShareEventArgs eventArgs;

        public event EventHandler<EventArgs> SharingComplete;

        public void ShowSharingDialog()
        {
            Process.Start(string.Format("http://facebook.com/sharer.php?s=100&p[url]={0}&p[title]={1}&p[summary]={2}",
                                        Uri.EscapeUriString(this.eventArgs.Url.ToString()),
                                        Uri.EscapeDataString(this.eventArgs.Title),
                                        Uri.EscapeDataString(this.eventArgs.Description)));

            if (this.SharingComplete != null)
            {
                this.SharingComplete(this, new EventArgs());
            }
        }

        public void ShareGame(ShareEventArgs eventArgs)
        {
            this.sharingState = SharingState.Game;
            this.eventArgs = eventArgs;
        }

        public void ShareEvent(ShareEventArgs eventArgs)
        {
            this.sharingState = SharingState.Event;
            this.eventArgs = eventArgs;
        }

        private enum SharingState
        {
            Game,
            Event
        }
    }
}