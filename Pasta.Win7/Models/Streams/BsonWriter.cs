using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Json;
using Pasta.Models.Streams;

namespace Pasta.Win7.Models.Streams
{
    public sealed class BsonWriter : IStreamWriter
    {
        public byte[] Write<T>(T data)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            byte[] compressedBytes;
            using (var stream = new MemoryStream())
            {
                using (var compressedStream = new GZipStream(stream, CompressionMode.Compress, true))
                {
                    serializer.WriteObject(compressedStream, data);
                }

                compressedBytes = stream.ToArray();
            }

            return compressedBytes;
        }
    }
}