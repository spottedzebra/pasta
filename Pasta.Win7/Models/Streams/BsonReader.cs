﻿using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Json;
using Pasta.Models.Streams;

namespace Pasta.Win7.Models.Streams
{
    public sealed class BsonReader : IStreamReader
    {
        public T Read<T>(byte[] data)
        {
            if (data == null)
            {
                return default(T);
            }

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            T restoredData;
            using (var stream = new MemoryStream(data))
            {
                using (var decompressedStream = new GZipStream(stream, CompressionMode.Decompress, true))
                {
                    restoredData = (T)serializer.ReadObject(decompressedStream);
                }
            }

            return restoredData;
        }
    }
}