﻿using System;
using System.IO;
using Pasta.Models.History;
using Pasta.Models.Streams;

namespace Pasta.Win7.Models.History
{
    public class FileRepository : IRepository
    {
        private readonly string fileName;
        private readonly string directoryPath;

        public FileRepository(string containerName, string fileName)
        {
            this.directoryPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), containerName);
            this.fileName = Path.Combine(this.directoryPath, fileName + ".bson");
        }

        public void ReadWrapper(
            IStreamReader reader, 
            string key, 
            Action<HistoryWrapper, object> callback, 
            object state)
        {
            if (!Directory.Exists(this.directoryPath) || !File.Exists(this.fileName))
            {
                callback(null, state);
            }

            callback(reader.Read<HistoryWrapper>(File.ReadAllBytes(this.fileName)), state);
        }

        public void WriteWrapper(IStreamWriter writer, string key, HistoryWrapper wrapper)
        {
            if (!Directory.Exists(this.directoryPath))
            {
                Directory.CreateDirectory(this.directoryPath);
            }

            using (var stream = File.Open(this.fileName, FileMode.Create))
            {
                byte[] bytes = writer.Write(wrapper);
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        public void ClearWrapper(string key)
        {
            if (Directory.Exists(this.directoryPath) && File.Exists(this.fileName))
            {
                File.Delete(this.fileName);
            }
        }
    }
}