﻿using System;
using System.IO;
using Pasta.Models.History;

namespace Pasta.Win7.Models.History
{
    public abstract class Win7HistoryUpgraderBase : IHistoryUpgrader
    {
        private readonly string directoryPath;
        private readonly string filePath;

        protected Win7HistoryUpgraderBase(string containerName, string fileName)
        {
            this.directoryPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), containerName);
            this.filePath = Path.Combine(this.directoryPath, fileName);
        }

        public bool CanUpgrade()
        {
            if (!Directory.Exists(this.directoryPath) || !File.Exists(this.filePath))
            {
                return false;
            }

            return this.CanUpgradeBytes(this.filePath);
        }

        public void Upgrade()
        {
            this.UpgradeBytes(this.filePath);
        }

        protected abstract bool CanUpgradeBytes(string filePath);
        protected abstract void UpgradeBytes(string filePath);
    }
}