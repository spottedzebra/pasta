﻿using System;

namespace Pasta.ViewModels
{
    public interface IConductorViewModel
    {
        event EventHandler<NavigationEventArgs> ViewModelPushed;
        event EventHandler<EventArgs> ViewModelPopped;
        event EventHandler<NavigationEventArgs> ViewModelSetTop;

        bool CanPop { get; }

        void Push(ConductorArguments arguments);
        void Pop();
        void SetTop(ConductorArguments arguments);
    }
}