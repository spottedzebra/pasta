using System;

namespace Pasta.ViewModels
{
    public class NavigationEventArgs : EventArgs
    {
        public NavigationEventArgs(IConductableViewModel targetViewModel)
        {
            this.TargetViewModel = targetViewModel;
        }

        public IConductableViewModel TargetViewModel { get; private set; }
    }
}