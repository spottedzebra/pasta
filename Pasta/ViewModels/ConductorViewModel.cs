using System;
using System.Collections.Generic;

namespace Pasta.ViewModels
{
    public class ConductorViewModel : IConductorViewModel
    {
        private readonly IConductableViewModelFactory viewModelFactory;
        public event EventHandler<NavigationEventArgs> ViewModelPushed;
        public event EventHandler<EventArgs> ViewModelPopped;
        public event EventHandler<NavigationEventArgs> ViewModelSetTop;

        private readonly Stack<IConductableViewModel> viewModels;

        public ConductorViewModel(IConductableViewModelFactory viewModelFactory)
        {
            this.viewModelFactory = viewModelFactory;
            this.viewModels = new Stack<IConductableViewModel>();
        }

        public bool CanPop
        {
            get { return this.viewModels.Count > 0; }
        }

        public void Push(ConductorArguments arguments)
        {
            IConductableViewModel viewModel = this.viewModelFactory.CreateViewModel(arguments);
            this.viewModels.Push(viewModel);
            if (this.ViewModelPushed != null)
            {
                this.ViewModelPushed(this, new NavigationEventArgs(viewModel));
            }
        }

        public void Pop()
        {
            this.viewModels.Pop();
            if (this.ViewModelPopped != null)
            {
                this.ViewModelPopped(this, new EventArgs());
            }
        }

        public void SetTop(ConductorArguments arguments)
        {
            this.viewModels.Clear();
            IConductableViewModel viewModel = this.viewModelFactory.CreateViewModel(arguments);
            this.viewModels.Push(viewModel);
            if (this.ViewModelSetTop != null)
            {
                this.ViewModelSetTop(this, new NavigationEventArgs(viewModel));
            }
        }
    }
}