﻿using System;

namespace Pasta.ViewModels
{
    public interface IConductableViewModelFactory
    {
        IConductableViewModel CreateViewModel(ConductorArguments arguments);
    }
}