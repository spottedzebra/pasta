﻿using System;

namespace Pasta.ViewModels
{
    public class ConductorArguments
    {
        public ConductorArguments(Type targetType)
        {
            this.TargetType = targetType;
        }

        public Type TargetType { get; private set; }
        public bool HasArguments { get; protected set; }
    }

    public class ConductorArguments<T1, T2, T3, T4> : ConductorArguments
    {
        public ConductorArguments(Type targetType, T1 argument1, T2 argument2, T3 argument3, T4 argument4)
            : base(targetType)
        {
            this.Argument1 = argument1;
            this.Argument2 = argument2;
            this.Argument3 = argument3;
            this.Argument4 = argument4;
            this.HasArguments = true;
        }

        public T1 Argument1 { get; private set; }

        public T2 Argument2 { get; private set; }

        public T3 Argument3 { get; private set; }

        public T4 Argument4 { get; private set; }
    }

    public class ConductorArguments<T1, T2, T3> : ConductorArguments
    {
        public ConductorArguments(Type targetType, T1 argument1, T2 argument2, T3 argument3)
            : base(targetType)
        {
            this.Argument1 = argument1;
            this.Argument2 = argument2;
            this.Argument3 = argument3;
            this.HasArguments = true;
        }

        public T1 Argument1 { get; private set; }

        public T2 Argument2 { get; private set; }

        public T3 Argument3 { get; private set; }
    }

    public class ConductorArguments<T1, T2> : ConductorArguments
    {
        public ConductorArguments(Type targetType, T1 argument1, T2 argument2)
            : base(targetType)
        {
            this.Argument1 = argument1;
            this.Argument2 = argument2;
            this.HasArguments = true;
        }

        public T1 Argument1 { get; private set; }

        public T2 Argument2 { get; private set; }
    }

    public class ConductorArguments<T> : ConductorArguments
    {
        public ConductorArguments(Type targetType, T argument)
            : base(targetType)
        {
            this.Argument = argument;
            this.HasArguments = true;
        }

        public T Argument { get; private set; }
    }
}