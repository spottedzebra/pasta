﻿
namespace Pasta.ViewModels
{
    public interface IUrlLauncher
    {
        void LaunchUrl(string url);
    }
}
