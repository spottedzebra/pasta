using System;

namespace Pasta.Models
{
    public class NullCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return false;
        }

        public void Execute(object parameter)
        {
        }
    }

    public class BasicCommand : ICommand
    {
        private readonly Action callback;
        private readonly Func<bool> canExecute;

        public BasicCommand(Action callback)
            : this(callback, null)
        {
        }

        public BasicCommand(Action callback, Func<bool> canExecute)
        {
            this.callback = callback;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute();
        }

        public void Execute(object parameter)
        {
            if (this.CanExecute(parameter))
            {
                this.callback();
            }
        }
    }

    public class BasicCommand<T> : ICommand
    {
        private readonly Action<T> callback;
        private readonly Func<T, bool> canExecute;

        public BasicCommand(Action<T> callback)
            : this(callback, null)
        {

        }

        public BasicCommand(Action<T> callback, Func<T, bool> canExecute)
        {
            this.callback = callback;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute((T)parameter);
        }

        public void Execute(object parameter)
        {
            if (this.CanExecute(parameter))
            {
                this.callback((T)parameter);
            }
        }
    }
}