namespace Pasta.Models.Streams
{
    public interface IStreamReader
    {
        T Read<T>(byte[] data);
    }
}