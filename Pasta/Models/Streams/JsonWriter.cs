using System.IO;
using System.Runtime.Serialization.Json;

namespace Pasta.Models.Streams
{
    public sealed class JsonWriter :IStreamWriter
    {
        public byte[] Write<T>(T data)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            byte[] bytes;
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, data);
                bytes = stream.ToArray();
            }

            return bytes;
        }
    }
}