namespace Pasta.Models.Streams
{
    public interface IStreamWriter
    {
        byte[] Write<T>(T data);
    }
}