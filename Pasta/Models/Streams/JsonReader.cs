﻿using System.IO;
using System.Runtime.Serialization.Json;

namespace Pasta.Models.Streams
{
    public sealed class JsonReader : IStreamReader
    {
        public T Read<T>(byte[] data)
        {
            if (data == null)
            {
                return default(T);
            }

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            T restoredData;
            using (MemoryStream stream = new MemoryStream(data))
            {
                restoredData = (T)serializer.ReadObject(stream);
            }

            return restoredData;
        }
    }
}
