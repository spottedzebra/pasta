﻿namespace Pasta.Models.Notifications
{
    public interface ITileManager<in T>
    {
        void UpdateTile(T data);
        void ResetTile();
    }
}
