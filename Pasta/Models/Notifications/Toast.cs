using System;

namespace Pasta.Models.Notifications
{
    public sealed class Toast
    {
        private readonly string message;
        private readonly Action callback;

        public Toast(string message, Action callback)
        {
            this.message = message;
            this.callback = callback;
        }

        public string Message
        {
            get { return this.message; }
        }

        public Action Callback
        {
            get { return this.callback; }
        }
    }
}