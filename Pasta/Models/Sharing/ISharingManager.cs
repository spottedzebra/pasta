﻿using System;

namespace Pasta.Models.Sharing
{
    public interface ISharingManager
    {
        event EventHandler<EventArgs> SharingComplete;

        void ShowSharingDialog();
        void ShareGame(ShareEventArgs eventArgs);
        void ShareEvent(ShareEventArgs eventArgs);
    }
}