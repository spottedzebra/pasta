﻿using System;

namespace Pasta.Models.Sharing
{
    public abstract class ShareEventArgs : EventArgs
    {
        public abstract string Title { get; }

        public abstract string Description { get; }

        public abstract Uri Url { get; }
    }
}