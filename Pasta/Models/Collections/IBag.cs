﻿using System.Collections.Generic;

namespace Pasta.Models.Collections
{
    public interface IBag<T> : IEnumerable<T>
    {
        int Count { get; }

        void Add(T item);
        bool Remove(T item);
        void Clear();
    }
}