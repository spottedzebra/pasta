﻿using System;
using System.Collections.Generic;

namespace Pasta.Models.Collections
{
    public interface IObservableBag<T> : IEnumerable<T>
    {
        event EventHandler<CollectionChangeEventArgs<T>> ItemAdded;
        event EventHandler<CollectionChangeEventArgs<T>> ItemRemoved;
        event EventHandler<CollectionChangeEventArgs> ItemsCleared;
    }
}