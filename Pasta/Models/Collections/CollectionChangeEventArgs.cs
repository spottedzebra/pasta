using System;
using System.Collections.Generic;

namespace Pasta.Models.Collections
{
    public sealed class CollectionChangeEventArgs : EventArgs
    {
    }

    public sealed class CollectionChangeEventArgs<T> : EventArgs
    {
        private readonly IEnumerable<T> items;

        public CollectionChangeEventArgs(params T[] items)
        {
            this.items = items;
        }

        public IEnumerable<T> Items
        {
            get { return this.items; }
        }
    }
}