using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Pasta.Models.Collections
{
    public sealed class ObservableBag<T> :IObservableBag<T>, IBag<T>
    {
        public event EventHandler<CollectionChangeEventArgs<T>> ItemAdded;
        public event EventHandler<CollectionChangeEventArgs<T>> ItemRemoved;
        public event EventHandler<CollectionChangeEventArgs> ItemsCleared;

        private readonly List<T> items;
 
        public ObservableBag()
        {
            this.items = new List<T>();
        }

        public T this[int index]
        {
            get { return this.items[index]; }
        }

        public int Count
        {
            get { return this.items.Count; }
        }

        public void Add(T item)
        {
            this.items.Add(item);
            if (this.ItemAdded != null)
            {
                this.ItemAdded(this, new CollectionChangeEventArgs<T>(item));
            }
        }

        public void AddRange(IEnumerable<T> collection)
        {
            this.items.AddRange(collection);
            if (this.ItemAdded != null)
            {
                this.ItemAdded(this, new CollectionChangeEventArgs<T>(collection.ToArray()));
            }
        }

        public bool Remove(T item)
        {
            bool wasItemRemoved = this.items.Remove(item);
            if (wasItemRemoved && this.ItemRemoved != null)
            {
                this.ItemRemoved(this, new CollectionChangeEventArgs<T>(item));
            }

            return wasItemRemoved;
        }

        public void RemoveAt(int index)
        {
            T item = this.items[index];
            this.items.RemoveAt(index);
            if (this.ItemRemoved != null)
            {
                this.ItemRemoved(this, new CollectionChangeEventArgs<T>(item));
            }
        }

        public void Clear()
        {
            this.items.Clear();
            if (this.ItemsCleared != null)
            {
                this.ItemsCleared(this, new CollectionChangeEventArgs());
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}