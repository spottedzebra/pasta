﻿namespace Pasta.Models.Messaging
{
    public class Message
    {
        public readonly string Category;
        public readonly string Value;

        public Message(string category, string value)
        {
            this.Category = category;
            this.Value = value;
        }
    }

    public class Message<T> : Message
    {
        public readonly T Argument;

        public Message(string category, string value, T argument)
            : base(category, value)
        {
            this.Argument = argument;
        }
    }
}