﻿using System;
using System.Collections.Generic;

namespace Pasta.Models.Messaging
{
    public class MessageQueue
    {
        private readonly Dictionary<string, List<Action<Message>>> processors;

        public MessageQueue()
        {
            this.processors = new Dictionary<string, List<Action<Message>>>();
        }

        public void AddProcessor(string category, Action<Message> processor)
        {
            if (!this.processors.ContainsKey(category))
            {
                this.processors[category] = new List<Action<Message>>();
            }

            this.processors[category].Add(processor);
        }

        public void RemoveProcessor(string category, Action<Message> processor)
        {
            if (this.processors.ContainsKey(category))
            {
                this.processors[category].Remove(processor);
            }
        }

        public void AddMessage(Message message)
        {
            if (!this.processors.ContainsKey(message.Category))
            {
                return;
            }

            var categoryProcessors = this.processors[message.Category].ToArray();
            foreach (var processor in categoryProcessors)
            {
                processor(message);
            }
        }

        public void Destroy()
        {
            this.processors.Clear();
        }
    }
}