using System;

namespace Pasta.Models.Tweens
{
    public class Tween : ITween
    {
        public delegate float EasingFunction(
            float startingValue,
            float targetValue,
            TimeSpan targetRunTime,
            TimeSpan elapsedTime);

        private float start;
        private float target;
        private readonly TimeSpan targetRunTime;
        private readonly EasingFunction easingFunction;
        private TimeSpan delayTimer;
        private TimeSpan elapsedTime;
        private int currentIteration;
        private float value;
        private bool isFirstRun;

        public Tween(
            float start, 
            float target, 
            TimeSpan targetRunTime,
            EasingFunction easingFunction)
        {
            this.target = target;
            this.targetRunTime = targetRunTime;
            this.start = this.Value = start;
            this.easingFunction = easingFunction;
            this.elapsedTime = TimeSpan.Zero;
            this.DelayType = DelayType.Always;
            this.isFirstRun = true;
        }

        public float Value
        {
            get
            {
                return this.value;
            }

            private set
            {
                if (Math.Abs(this.value - value) > 0.001f)
                {
                    this.value = value;
                    if (this.UpdateCallback != null)
                    {
                        this.UpdateCallback(this, this.Value);
                    }
                }
            }
        }

        public Repeat Repeats { get; set; }

        public int CurrentIteration
        {
            get { return this.currentIteration; }
        }

        public DelayType DelayType { get; set; }

        public TimeSpan RemainingDelay
        {
            get { return this.Delay - this.delayTimer; }
        }

        public bool YoYos { get; set; }

        public bool IsPaused { get; set; }

        public Action<ITween, float> UpdateCallback { get; set; }

        public Action<ITween, float> RepeatsCallback { get; set; }

        public Action<ITween, float> FinishCallback { get; set; }

        public TimeSpan Delay { get; set; }

        public bool IsFinished { get; private set; }

        public void Update(TimeSpan elapsedGameTime)
        {
            if (this.IsPaused)
            {
                return;
            }

            if (this.isFirstRun)
            {
                this.isFirstRun = false;
                if (this.DelayType == DelayType.OnlyOnRepeat || this.DelayType == DelayType.OnlyOnYoYo)
                {
                    this.delayTimer = this.Delay;
                }
            }

            TimeSpan delta = (this.delayTimer + elapsedGameTime) - this.Delay;
            if (delta < TimeSpan.Zero)
            {
                this.delayTimer += elapsedGameTime;
                return;
            }
            else
            {
                this.delayTimer = this.Delay;
            }

            this.elapsedTime += delta;
            if (this.elapsedTime.CompareTo(this.targetRunTime) < 0)
            {
                this.Value = this.easingFunction(this.start, this.target, this.targetRunTime, this.elapsedTime);
            }
            else if (!this.IsFinished)
            {
                bool shouldRepeat = this.Repeats == Repeat.Forever || this.Repeats.NumberOfRepetitions > this.currentIteration;
                if (shouldRepeat)
                {
                    if (this.RepeatsCallback != null)
                    {
                        this.RepeatsCallback(this, this.target);
                    }

                    this.currentIteration++;
                    if (this.YoYos)
                    {
                        if (this.DelayType == DelayType.OnlyOnYoYo)
                        {
                            this.delayTimer = TimeSpan.Zero;
                        }

                        this.Reverse();
                    }
                    else
                    {
                        if (this.DelayType == DelayType.Always || this.DelayType == DelayType.OnlyOnRepeat)
                        {
                            this.delayTimer = TimeSpan.Zero;
                        }

                        while (this.elapsedTime >= this.targetRunTime)
                        {
                            this.elapsedTime = this.elapsedTime.Subtract(this.targetRunTime);
                        }

                        this.Value = this.easingFunction(this.start, this.target, this.targetRunTime, this.elapsedTime);
                    }
                }
                else
                {
                    this.Value = this.target;
                    this.IsFinished = true;
                    if (this.FinishCallback != null)
                    {
                        this.FinishCallback(this, this.Value);
                    }
                }
            }
        }

        public void Restart()
        {
            this.IsPaused = false;
            this.IsFinished = false;
            this.elapsedTime = TimeSpan.Zero;
            this.delayTimer = TimeSpan.Zero;
            this.currentIteration = 0;
            this.Value = this.start;
        }

        /// <summary>
        /// Flips start and end values and adjusts elapsed time to reflect changes.
        /// </summary>
        private void Reverse()
        {
            float originalStart = this.start;
            this.start = this.target;
            this.target = originalStart;
            this.elapsedTime = this.targetRunTime.Subtract(this.elapsedTime);
        }
    }
}