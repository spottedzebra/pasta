namespace Pasta.Models.Tweens
{
    public struct Repeat
    {
        public static readonly Repeat Never = new Repeat(0);
        public static readonly Repeat Once = new Repeat(1);
        public static readonly Repeat Twice = new Repeat(2);
        public static readonly Repeat Forever = new Repeat(-1);

        public readonly int NumberOfRepetitions;

        public Repeat(int numberOfRepetitions)
        {
            this.NumberOfRepetitions = numberOfRepetitions;
        }

        public bool Equals(Repeat other)
        {
            return this.NumberOfRepetitions == other.NumberOfRepetitions;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return this.NumberOfRepetitions;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Repeat && Equals((Repeat)obj);
        }

        public static bool operator ==(Repeat one, Repeat two)
        {
            return one.Equals(two);
        }

        public static bool operator !=(Repeat one, Repeat two)
        {
            return !(one == two);
        }
    }
}