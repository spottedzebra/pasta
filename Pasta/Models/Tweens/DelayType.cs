﻿namespace Pasta.Models.Tweens
{
    public enum DelayType
    {
        Always,
        OnlyOnFirstRun,
        OnlyOnRepeat,
        OnlyOnYoYo,
    }
}