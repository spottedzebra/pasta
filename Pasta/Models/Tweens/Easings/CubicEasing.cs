﻿using System;

namespace Pasta.Models.Tweens.Easings
{
    public class CubicEasing : IEasing
    {
        public float EaseIn(float startingValue, float targetValue, TimeSpan targetRunTime, TimeSpan elapsedTime)
        {
            float time = (float)(elapsedTime.TotalSeconds / targetRunTime.TotalSeconds);
            float delta = targetValue - startingValue;
            return delta * time * time * time + startingValue;
        }

        public float EaseOut(float startingValue, float targetValue, TimeSpan targetRunTime, TimeSpan elapsedTime)
        {
            float time = (float)(elapsedTime.TotalSeconds / targetRunTime.TotalSeconds);
            time -= 1;
            float delta = targetValue - startingValue;
            return delta * (time * time * time + 1) + startingValue;
        }
    }
}