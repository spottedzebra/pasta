﻿using System;

namespace Pasta.Models.Tweens.Easings
{
    public class QuadraticEasing : IEasing
    {
        public float EaseIn(float startingValue, float targetValue, TimeSpan targetRunTime, TimeSpan elapsedTime)
        {
            float time = (float)(elapsedTime.TotalSeconds / targetRunTime.TotalSeconds);
            float delta = targetValue - startingValue;
            return delta * time * time + startingValue;
        }

        public float EaseOut(float startingValue, float targetValue, TimeSpan targetRunTime, TimeSpan elapsedTime)
        {
            float time = (float)(elapsedTime.TotalSeconds / targetRunTime.TotalSeconds);
            float delta = targetValue - startingValue;
            return -delta * time * (time - 2f) + startingValue;
        }
    }
}