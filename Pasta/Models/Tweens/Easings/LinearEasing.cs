﻿using System;

namespace Pasta.Models.Tweens.Easings
{
    public class LinearEasing : IEasing
    {
        public float EaseIn(float startingValue, float targetValue, TimeSpan targetRunTime, TimeSpan elapsedTime)
        {
            float delta = targetValue - startingValue;
            return (float)(delta * elapsedTime.TotalSeconds / targetRunTime.TotalSeconds + startingValue);
        }

        public float EaseOut(float startingValue, float targetValue, TimeSpan targetRunTime, TimeSpan elapsedTime)
        {
            float delta = targetValue - startingValue;
            return (float)(delta * elapsedTime.TotalSeconds / targetRunTime.TotalSeconds + startingValue);
        }
    }
}