using System;

namespace Pasta.Models.Tweens.Easings
{
    public interface IEasing
    {
        float EaseIn(
            float startingValue,
            float targetValue,
            TimeSpan targetRunTime,
            TimeSpan elapsedTime);

        float EaseOut(
            float startingValue,
            float targetValue,
            TimeSpan targetRunTime,
            TimeSpan elapsedTime);
    }
}