using System;

namespace Pasta.Models.Tweens.Easings
{
    public class DiscreteEasing : IEasing
    {
        public float EaseIn(float startingValue, float targetValue, TimeSpan targetRunTime, TimeSpan elapsedTime)
        {
            int delta = (int)(targetValue - startingValue);
            float percentComplete = (float)(elapsedTime.TotalSeconds / targetRunTime.TotalSeconds);
            return (float)(startingValue + Math.Floor(delta * percentComplete));

            return elapsedTime < targetRunTime ? startingValue : targetValue;
        }

        public float EaseOut(float startingValue, float targetValue, TimeSpan targetRunTime, TimeSpan elapsedTime)
        {
            throw new NotSupportedException();
        }
    }
}