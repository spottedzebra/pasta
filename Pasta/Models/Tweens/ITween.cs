using System;

namespace Pasta.Models.Tweens
{
    /// <summary>
    /// A utility class which makes it easy to interpolate between two values.
    /// </summary>
    /// <remarks>
    /// This class is used to create simple animations programatically. For instance
    /// to fade a screen into view or scale a button on click.
    /// </remarks>
    public interface ITween
    {
        float Value { get; }
        Repeat Repeats { get; set; }
        int CurrentIteration { get; }
        DelayType DelayType { get; set; }
        TimeSpan Delay { get; set; }
        TimeSpan RemainingDelay { get; }
        bool YoYos { get; set; }
        bool IsPaused { get; set; }
        bool IsFinished { get; }
        Action<ITween, float> UpdateCallback { get; set; }
        Action<ITween, float> RepeatsCallback { get; set; }
        Action<ITween, float> FinishCallback { get; set; }

        void Update(TimeSpan elapsedGameTime);
        void Restart();
    }
}