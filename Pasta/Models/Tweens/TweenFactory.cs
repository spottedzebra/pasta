using System;
using Pasta.Models.Tweens.Easings;

namespace Pasta.Models.Tweens
{
    public static class TweenFactory
    {
        public static ITween Tween(float start, float target, TimeSpan targetRunTime)
        {
            return TweenFactory.Tween(start, target, targetRunTime, new LinearEasing().EaseIn);
        }

        public static ITween Tween(float start, float target, TimeSpan targetRunTime, Tween.EasingFunction easingFunction)
        {
            return new Tween(start, target, targetRunTime, easingFunction);
        }
    }
}