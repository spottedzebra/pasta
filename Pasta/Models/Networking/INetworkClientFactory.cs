﻿namespace Pasta.Models.Networking
{
    public interface INetworkClientFactory
    {
        INetworkClient CreateClient(string playerId);
    }
}