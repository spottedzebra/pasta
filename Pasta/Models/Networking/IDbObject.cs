﻿using System;

namespace Pasta.Models.Networking
{
    public interface IDbObject
    {
        byte[] Get(string propertyName);
        void Set(string propertyName, byte[] value);
        void Set(string propertyName, string value);
        void Set(string propertyName, int value);
        void Set(string propertyName, DateTime value);
        void Save();
    }
}