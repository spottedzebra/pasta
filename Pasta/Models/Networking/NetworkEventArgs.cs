﻿using System;

namespace Pasta.Models.Networking
{
    public class NetworkEventArgs : EventArgs
    {
        public NetworkEventArgs(NetworkMessage message)
        {
            this.Message = message;
        }

        public NetworkMessage Message { get; private set; }
    }
}