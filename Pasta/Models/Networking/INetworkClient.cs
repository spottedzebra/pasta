﻿using System;

namespace Pasta.Models.Networking
{
    public interface INetworkClient
    {
        event EventHandler<NetworkEventArgs> OnMessage;

        string Id { get; }

        void Send(NetworkMessage message);
        void LoadPlayerObject(Action<IDbObject> callback);
        void LoadFirst(string tableName, string indexName, object indexArg, Action<IDbObject> callback);
        void LoadOrCreate(string tableName, string key, Action<IDbObject> callback);
        void Delete(string tableName, string key);
    }
}