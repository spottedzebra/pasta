﻿namespace Pasta.Models.Networking
{
    public class NetworkMessage
    {
        public NetworkMessage(string type, params object[] arguments)
        {
            this.Type = type;
            this.Arguments = arguments;
        }

        public string Type { get; private set; }

        public object[] Arguments { get; private set; }

        public int GetArg(int i)
        {
            return (int)this.Arguments[i];
        }
    }
}