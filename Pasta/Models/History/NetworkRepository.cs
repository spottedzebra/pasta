﻿using System;
using Pasta.Models.Networking;
using Pasta.Models.Streams;

namespace Pasta.Models.History
{
    public class NetworkRepository : IRepository
    {
        private readonly INetworkClient client;
        private readonly string tableName;

        public NetworkRepository(
            INetworkClient client,
            string tableName)
        {
            this.client = client;
            this.tableName = tableName;
        }

        public void ReadWrapper(
            IStreamReader reader, 
            string key,
            Action<HistoryWrapper, object> callback,
            object state)
        {
            this.client.LoadOrCreate(this.tableName, key, o =>
                                                              {
                                                                  var wrapper = reader.Read<HistoryWrapper>(o.Get("Wrapper"));
                                                                  callback(wrapper, state);
                                                              });
        }

        public void WriteWrapper(IStreamWriter writer, string key, HistoryWrapper wrapper)
        {
            this.client.LoadOrCreate(this.tableName, key, o =>
                                                              {
                                                                  o.Set("Owner", this.client.Id);
                                                                  o.Set("DateChanged", wrapper.DateCreated);
                                                                  o.Set("Version", wrapper.Version);
                                                                  o.Set("Wrapper", writer.Write(wrapper));
                                                                  o.Save();
                                                              });
        }

        public void ClearWrapper(string key)
        {
            this.client.Delete(this.tableName, key);
        }
    }
}