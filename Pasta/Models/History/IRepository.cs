﻿using System;
using Pasta.Models.Streams;

namespace Pasta.Models.History
{
    public interface IRepository
    {
        /// <summary>
        /// Attempts to read a wrapper from storage.
        /// </summary>
        /// <returns>A stored wrapper or null.</returns>
        void ReadWrapper(IStreamReader reader, string key, Action<HistoryWrapper, object> callback, object state);

        /// <summary>
        /// Writes the given wrapper to storage.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="key">Unique key identifying the wrapper</param>
        /// <param name="wrapper">An updated wrapper</param>
        void WriteWrapper(IStreamWriter writer, string key, HistoryWrapper wrapper);

        /// <summary>
        /// Removes the wrapper entirely.
        /// </summary>
        /// <param name="key"></param>
        void ClearWrapper(string key); 
    }
}