﻿using System;
using Pasta.Models.Networking;
using Pasta.Models.Streams;

namespace Pasta.Models.History
{
    public class NetworkPlayerRepository : IRepository
    {
        private readonly INetworkClient client;

        public NetworkPlayerRepository(
            INetworkClient client)
        {
            this.client = client;
        }

        public void ReadWrapper(
            IStreamReader reader, 
            string key,
            Action<HistoryWrapper, object> callback,
            object state)
        {
            this.client.LoadPlayerObject(o =>
                                             {
                                                 var wrapper = reader.Read<HistoryWrapper>(o.Get("Wrapper"));
                                                 callback(wrapper, state);
                                             });
        }

        public void WriteWrapper(IStreamWriter writer, string key, HistoryWrapper wrapper)
        {
            this.client.LoadPlayerObject(o =>
                                             {
                                                 o.Set("Owner", this.client.Id);
                                                 o.Set("DateChanged", wrapper.DateCreated);
                                                 o.Set("Version", wrapper.Version);
                                                 o.Set("Wrapper", writer.Write(wrapper));
                                                 o.Save();
                                             });
        }

        public void ClearWrapper(string key)
        {
            // cannot delete player object
        }
    }
}