using System;
using System.Collections.Generic;
using Pasta.Models.Streams;

namespace Pasta.Models.History
{
    public sealed class HistoryManager<T> : IHistoryManager<T>
    {
        private readonly int version;
        private readonly IEnumerable<IHistoryUpgrader> upgraders;
        private readonly IStreamReader reader;
        private readonly IStreamWriter writer;

        public HistoryManager(
            int version, 
            IStreamReader reader, 
            IStreamWriter writer,
            params IHistoryUpgrader[] upgraders)
        {
            this.reader = reader;
            this.writer = writer;
            this.upgraders = upgraders;
            this.version = version;
        }

        public void ReadHistory(IRepository repository, string key, Action<T> callback)
        {
#if RELEASE
            try
            {
#endif
                this.UpgradeLegacyHistory();

                repository.ReadWrapper(this.reader, key, this.OnWrapperRead, callback);
#if RELEASE
            }
            catch (Exception e)
            {
                // in the event that the session is totally beyond recognition just clear it and start from scratch.
                this.ClearHistory(historyObject);
            }
#endif
        }

        private void OnWrapperRead(HistoryWrapper wrapper, object state)
        {
            T value = wrapper != null ? this.reader.Read<T>(wrapper.Data) : default(T);
            var callback = (Action<T>)state;
            callback(value);
        }

        public void WriteHistory(IRepository repository, string key, T historyObject)
        {
            HistoryWrapper wrapper = new HistoryWrapper();
            wrapper.Version = this.version;
            wrapper.DateCreated = DateTime.UtcNow;
            wrapper.Data = this.writer.Write(historyObject);

            repository.WriteWrapper(this.writer, key, wrapper);
        }

        public void ClearHistory(IRepository repository, string key)
        {
            repository.ClearWrapper(key);
        }

        private void UpgradeLegacyHistory()
        {
            foreach (var upgrader in this.upgraders)
            {
                if (upgrader.CanUpgrade())
                {
                    upgrader.Upgrade();
                }
            }
        }
    }
}