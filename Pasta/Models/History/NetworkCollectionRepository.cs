﻿using System;
using Pasta.Models.Networking;
using Pasta.Models.Streams;

namespace Pasta.Models.History
{
    public class NetworkCollectionRepository : IRepository
    {
        private readonly INetworkClient client;
        private readonly string tableName;
        private readonly string indexName;

        public NetworkCollectionRepository(
            INetworkClient client,
            string tableName, 
            string indexName)
        {
            this.client = client;
            this.tableName = tableName;
            this.indexName = indexName;
        }

        public void ReadWrapper(
            IStreamReader reader, 
            string key, 
            Action<HistoryWrapper, object> callback, 
            object state)
        {
            this.client.LoadFirst(this.tableName, this.indexName, key, o =>
                                                                           {
                                                                               var wrapper = reader.Read<HistoryWrapper>(o.Get("Wrapper"));
                                                                               callback(wrapper, state);
                                                                           });
        }

        public void WriteWrapper(IStreamWriter writer, string key, HistoryWrapper wrapper)
        {
            this.client.LoadOrCreate(
                this.tableName,
                key,
                o =>
                    {
                        o.Set("Owner", this.client.Id);
                        o.Set("DateChanged", wrapper.DateCreated);
                        o.Set("Version", wrapper.Version);
                        o.Set("Wrapper", writer.Write(wrapper));
                        o.Save();
                    });
        }

        public void ClearWrapper(string key)
        {
            this.client.Delete(this.tableName, key);
        }
    }
}