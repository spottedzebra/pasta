using System;

namespace Pasta.Models.History
{
    public sealed class HistoryWrapper
    {
        public int Version { get; set; }
        public DateTime DateCreated { get; set; }
        public byte[] Data { get; set; }
    }
}