﻿using System;

namespace Pasta.Models.History
{
    public interface IHistoryManager<T>
    {
        void ReadHistory(IRepository repository, string key, Action<T> callback);
        void WriteHistory(IRepository repository, string key, T historyObject);
        void ClearHistory(IRepository repository, string key);
    }
}