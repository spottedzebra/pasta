namespace Pasta.Models.History
{
    public interface IHistoryUpgrader
    {
        bool CanUpgrade();
        void Upgrade();
    }
}