﻿using Pasta.ViewModels;
using System;

namespace Pasta.Win8.ViewModels
{
    public class Win8UrlLauncher : IUrlLauncher
    {
        public async void LaunchUrl(string url)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri(url));
        }
    }
}
