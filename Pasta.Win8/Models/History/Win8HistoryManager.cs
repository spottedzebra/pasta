﻿using System;
using Pasta.Models.History;
using Pasta.Models.Streams;
using Windows.Storage;

namespace Pasta.Win8.Models.History
{
    public class Win8HistoryManager : IRepository
    {
        private readonly bool canRoam;

        public Win8HistoryManager(bool canRoam)
        {
            this.canRoam = canRoam;
        }

        private ApplicationDataContainer LocalSettings
        {
            get { return ApplicationData.Current.LocalSettings; }
        }

        private ApplicationDataContainer RoamingSettings
        {
            get { return ApplicationData.Current.RoamingSettings; }
        }

        public void ReadWrapper(
            IStreamReader reader,
            string key,
            Action<HistoryWrapper, object> callback,
            object state)
        {
            if (this.canRoam && this.RoamingSettings.Values.ContainsKey(key))
            {
                callback(reader.Read<HistoryWrapper>(
                    (byte[])this.RoamingSettings.Values[key]),
                         state);
            }

            if (this.LocalSettings.Values.ContainsKey(key))
            {
                callback(reader.Read<HistoryWrapper>(
                    (byte[])this.LocalSettings.Values[key]),
                         state);
            }
        }

        public void WriteWrapper(IStreamWriter writer, string key, HistoryWrapper wrapper)
        {
            this.LocalSettings.Values[key] = writer.Write(wrapper);
            if (this.canRoam)
            {
                this.RoamingSettings.Values[key] = writer.Write(wrapper);
            }

            ApplicationData.Current.SignalDataChanged();
        }

        public void ClearWrapper(string key)
        {
            if (this.LocalSettings.Values.ContainsKey(key))
            {
                this.LocalSettings.Values.Remove(key);
            }

            if (this.RoamingSettings.Values.ContainsKey(key))
            {
                this.RoamingSettings.Values.Remove(key);
            }

            ApplicationData.Current.SignalDataChanged();
        }
    }
}