﻿using Pasta.Models.History;
using Windows.Storage;

namespace Pasta.Win8.Models.History
{
    public abstract class Win8HistoryUpgraderBase : IHistoryUpgrader
    {
        private readonly bool testRoaming;

        protected Win8HistoryUpgraderBase(bool testRoaming)
        {
            this.testRoaming = testRoaming;
        }

        protected ApplicationDataContainer LocalSettings
        {
            get { return ApplicationData.Current.LocalSettings; }
        }

        protected ApplicationDataContainer RoamingSettings
        {
            get { return ApplicationData.Current.RoamingSettings; }
        }

        public bool CanUpgrade()
        {
            return this.CanUpgradeContainer(this.LocalSettings) ||
                   (this.testRoaming && this.CanUpgradeContainer(this.RoamingSettings));
        }

        public void Upgrade()
        {
            if (this.testRoaming && this.CanUpgradeContainer(this.RoamingSettings))
            {
                this.UpgradeContainer(this.RoamingSettings);
            }

            if (this.CanUpgradeContainer(this.LocalSettings))
            {
                this.UpgradeContainer(this.LocalSettings);
            }
        }

        protected abstract bool CanUpgradeContainer(ApplicationDataContainer container);
        protected abstract void UpgradeContainer(ApplicationDataContainer container);
    }
}