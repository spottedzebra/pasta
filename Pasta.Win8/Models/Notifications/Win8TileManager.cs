﻿using System;
using System.Linq;
using Pasta.Models.Notifications;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace Pasta.Win8.Models.Notifications
{
    public abstract class Win8TileManager<T> : ITileManager<T>
    {
        public void UpdateTile(T data)
        {
            XmlDocument tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWidePeekImage02);
            var image = tileXml.GetElementsByTagName("image").First();
            image.Attributes.GetNamedItem("src").InnerText = "ms-appx:/Assets/WideLogo.png";

            var text = tileXml.GetElementsByTagName("text");
            text.First().InnerText = this.GetTitleFor(data);
            text.Skip(1).First().InnerText = this.GetLine1For(data);
            text.Skip(2).First().InnerText = this.GetLine2For(data);

            TileNotification tileNotification = new TileNotification(tileXml);
            tileNotification.ExpirationTime = DateTimeOffset.UtcNow.AddDays(7);
            TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
        }

        public void ResetTile()
        {
            TileUpdateManager.CreateTileUpdaterForApplication().Clear();
        }

        protected abstract string GetTitleFor(T data);
        protected abstract string GetLine1For(T data);
        protected abstract string GetLine2For(T data);
    }
}
