﻿using System;
using Pasta.Models.Settings;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
using Pasta.Models;

namespace Pasta.Win8.Models.Settings
{
    public abstract class Win8SettingsManager : ISettingsManager
    {
        private readonly ICommand showSettingsCommand;
        private readonly ICommand showAboutCommand;
        private readonly ICommand showHelpCommand;
        private readonly Uri privacyUrl;

        protected Win8SettingsManager(
            ICommand showSettingsCommand,
            ICommand showAboutCommand,
            ICommand showHelpCommand,
            Uri privacyUrl)
        {
            this.showSettingsCommand = showSettingsCommand;
            this.showAboutCommand = showAboutCommand;
            this.showHelpCommand = showHelpCommand;
            this.privacyUrl = privacyUrl;
            SettingsPane.GetForCurrentView().CommandsRequested += this.OnSettingsRequested;
        }

        public void ShowSettings()
        {
            SettingsPane.Show();
        }

        private void OnSettingsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            SettingsCommand settingsCommand = new SettingsCommand("SettingsCommand", "Settings", this.ShowSettings);
            SettingsCommand aboutCommand = new SettingsCommand("AboutCommand", "About", this.ShowAbout);
            SettingsCommand helpCommand = new SettingsCommand("HelpCommand", "Help", this.ShowHelp);
            SettingsCommand privacyCommand = new SettingsCommand("PrivacyCommand", "Privacy Policy", this.ShowPrivacy);
            args.Request.ApplicationCommands.Add(settingsCommand);
            args.Request.ApplicationCommands.Add(aboutCommand);
            args.Request.ApplicationCommands.Add(helpCommand);
            args.Request.ApplicationCommands.Add(privacyCommand);
        }

        private void ShowSettings(IUICommand command)
        {
            this.showSettingsCommand.Execute(null);
        }

        private void ShowAbout(IUICommand command)
        {
            this.showAboutCommand.Execute(null);
        }

        private void ShowHelp(IUICommand command)
        {
            this.showHelpCommand.Execute(null);
        }

        private void ShowPrivacy(IUICommand command)
        {
            Windows.System.Launcher.LaunchUriAsync(this.privacyUrl);
        }
    }
}