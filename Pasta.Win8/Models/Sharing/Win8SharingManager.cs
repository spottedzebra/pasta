﻿using System;
using Pasta.Models.Sharing;
using Windows.ApplicationModel.DataTransfer;

namespace Pasta.Win8.Models.Sharing
{
    public class Win8SharingManager : ISharingManager
    {
        private SharingState sharingState;
        private ShareEventArgs eventArgs;

        public Win8SharingManager()
        {
            // TODO data is never returned
            this.sharingState = SharingState.Game;
            DataTransferManager.GetForCurrentView().DataRequested += this.OnDataRequested;
        }

        public event EventHandler<EventArgs> SharingComplete;

        public void ShowSharingDialog()
        {
            try
            {
                DataTransferManager.ShowShareUI();
            }
            catch (Exception)
            {
            }
        }

        public void ShareGame(ShareEventArgs eventArgs)
        {
            this.sharingState = SharingState.Game;
            this.eventArgs = eventArgs;
        }

        public void ShareEvent(ShareEventArgs eventArgs)
        {
            this.sharingState = SharingState.Event;
            this.eventArgs = eventArgs;
        }

        private void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            args.Request.Data.Destroyed += DataOnDestroyed;
            switch (this.sharingState)
            {
                case SharingState.Game:
                    args.Request.Data.Properties.Title = this.eventArgs.Title;
                    args.Request.Data.Properties.Description = this.eventArgs.Description;
                    args.Request.Data.SetUri(this.eventArgs.Url);
                    break;
                case SharingState.Event:
                    args.Request.Data.Properties.Title = this.eventArgs.Title;
                    args.Request.Data.SetText(this.eventArgs.Description);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void DataOnDestroyed(DataPackage sender, object args)
        {
            sender.Destroyed -= this.DataOnDestroyed;
            if (this.SharingComplete != null)
            {
                this.SharingComplete(this, new EventArgs());
            }
        }

        private enum SharingState
        {
            Game,
            Event
        }
    }
}