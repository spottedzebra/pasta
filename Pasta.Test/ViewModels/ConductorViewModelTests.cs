﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.ViewModels;

namespace Pasta.Test.ViewModels
{
    [TestClass]
    public class ConductorViewModelTests
    {
        private ConductorViewModel conductor;

        [TestInitialize]
        public void TestInitialize()
        {
            this.conductor = new ConductorViewModel();
        }

        [TestMethod]
        public void PushingViewModelFiresEvent()
        {
            bool pushedEventFired = false;
            Type pushedViewModel = null;
            this.conductor.ViewModelPushed += (sender, args) =>
                                                  {
                                                      pushedEventFired = true;
                                                      pushedViewModel = args.TargetViewModel;
                                                  };

            this.conductor.Push(typeof(TestViewModel));

            Assert.IsTrue(pushedEventFired);
            Assert.AreEqual(typeof(TestViewModel), pushedViewModel);
        }

        [TestMethod]
        public void PoppingViewModelFiresEvent()
        {
            bool poppedEventFired = false;
            this.conductor.ViewModelPopped += (sender, args) => poppedEventFired = true;

            this.conductor.Pop();

            Assert.IsTrue(poppedEventFired);
        }

        [TestMethod]
        public void SetTopViewModelFiresEvent()
        {
            bool setTopEventFired = false;
            Type setTopViewModel = null;
            this.conductor.ViewModelSetTop += (sender, args) =>
            {
                setTopEventFired = true;
                setTopViewModel = args.TargetViewModel;
            };

            this.conductor.SetTop(typeof(TestViewModel));

            Assert.IsTrue(setTopEventFired);
            Assert.AreEqual(typeof(TestViewModel), setTopViewModel);
        }
    }
}