﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models.Tweens;
using Pasta.Models.Tweens.Easings;

namespace Pasta.Test.Models.Tweens
{
    [TestClass]
    public class TweenTests
    {
        private Tween tween;

        [TestInitialize]
        public void TestInitialize()
        {
            this.tween = new Tween(0, 1, TimeSpan.FromSeconds(1), new LinearEasing().EaseIn);
        }

        [TestMethod]
        public void ANewTweenIsNotPaused()
        {
            Assert.IsFalse(this.tween.IsPaused);
        }

        [TestMethod]
        public void ANewTweenIsNotFinished()
        {
            Assert.IsFalse(this.tween.IsFinished);
        }

        [TestMethod]
        public void ANewTweenHasItsStartValue()
        {
            Assert.AreEqual(0, this.tween.Value);
        }

        [TestMethod]
        public void UpdatingTweenWhilePausedDoesNothing()
        {
            this.tween.IsPaused = true;

            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.tween.Value);
        }

        [TestMethod]
        public void UpdatingTweenWhileRunningUpdatesValue()
        {
            this.tween.Update(TimeSpan.FromSeconds(0.5f));

            Assert.AreEqual(0.5f, this.tween.Value);
        }

        [TestMethod]
        public void UpdatingATweenAllTheWayFinishesTheTween()
        {
            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(1, this.tween.Value);
            Assert.IsTrue(this.tween.IsFinished);
        }

        [TestMethod]
        public void RestartingATweenUnpausesTheTween()
        {
            this.tween.Restart();

            Assert.IsFalse(this.tween.IsPaused);
        }

        [TestMethod]
        public void RestartingATweenAfterItIsFinishedResetsItsValue()
        {
            this.tween.Restart();

            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Restart();

            Assert.AreEqual(0, this.tween.Value);
            Assert.IsFalse(this.tween.IsFinished);
        }

        [TestMethod]
        public void UpdatingInvokesCallback()
        {
            this.tween.Restart();
            bool wasCalled = false;
            this.tween.UpdateCallback = f => wasCalled = true;

            this.tween.Update(TimeSpan.FromSeconds(0.5f));

            Assert.IsTrue(wasCalled);
        }

        [TestMethod]
        public void FinishingTweenInvokesUpdateCallback()
        {
            bool wasCalled = false;
            this.tween.UpdateCallback = f => wasCalled = true;

            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.IsTrue(wasCalled);
        }

        [TestMethod]
        public void FinishingTweenInvokesCallback()
        {
            bool wasCalled = false;
            this.tween.FinishCallback = f => wasCalled = true;

            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.IsTrue(wasCalled);
        }

        [TestMethod]
        public void FinishingTweenOnlyInvokesCallbackOnce()
        {
            this.tween.Restart();
            int numberOfCalls = 0;
            this.tween.FinishCallback = f => numberOfCalls++;

            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(1, numberOfCalls);
        }

        [TestMethod]
        public void AddingAPreDelayDelaysTween1()
        {
            this.tween.PreDelay = TimeSpan.FromSeconds(1);

            this.tween.Update(TimeSpan.FromSeconds(0.5f));
            this.tween.Update(TimeSpan.FromSeconds(0.5f));

            Assert.AreEqual(0, this.tween.Value);
        }

        [TestMethod]
        public void AddingAPreDelayDelaysTween2()
        {
            this.tween.PreDelay = TimeSpan.FromSeconds(1);

            this.tween.Update(TimeSpan.FromSeconds(2));

            Assert.AreEqual(1, this.tween.Value);
        }

        [TestMethod]
        public void RestartingATweenResetsTheDelay()
        {
            this.tween.PreDelay = TimeSpan.FromSeconds(1);

            this.tween.Update(TimeSpan.FromSeconds(2));
            this.tween.Restart();
            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.tween.Value);
        }
    }
}