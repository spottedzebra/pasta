﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models.Tweens;
using Pasta.Models.Tweens.Easings;

namespace Pasta.Test.Models.Tweens
{
    [TestClass]
    public class TweenFactoryTests
    {
        [TestMethod]
        public void CreatingATweenReturnsNewTween()
        {
            var tween = TweenFactory.Tween(0, 1, TimeSpan.FromSeconds(1));

            tween.Update(TimeSpan.FromSeconds(0.5f));

            Assert.AreEqual(0.5f, tween.Value);
        }

        [TestMethod]
        public void CreatingATweenWithEasingReturnsNewTween()
        {
            var tween = TweenFactory.Tween(0, 1, TimeSpan.FromSeconds(1), new DiscreteEasing().EaseIn);

            tween.Update(TimeSpan.FromMilliseconds(0.5f));

            Assert.AreEqual(0, tween.Value);
        }
    }
}