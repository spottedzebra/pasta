﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models.Tweens;
using Pasta.Models.Tweens.Easings;

namespace Pasta.Test.Models.Tweens
{
    [TestClass]
    public class TweenRepetitionTests
    {
        private Tween tween;

        [TestInitialize]
        public void TestInitialize()
        {
            this.tween = new Tween(0, 1, TimeSpan.FromSeconds(1), new LinearEasing().EaseIn);
        }

        [TestMethod]
        public void ARepeatingTweenResetsOnFinish()
        {
            this.tween.Repeats = Repeat.Once;

            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.tween.Value);
            Assert.IsFalse(this.tween.IsFinished);
        }

        [TestMethod]
        public void AForeverRepeatingTweenResetsOnFinish()
        {
            this.tween.Repeats = Repeat.Forever;

            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.tween.Value);
            Assert.IsFalse(this.tween.IsFinished);
        }

        [TestMethod]
        public void OverageTimeIsCountedOnRepition()
        {
            this.tween.Repeats = Repeat.Once;

            this.tween.Update(TimeSpan.FromSeconds(1.5f));

            Assert.AreEqual(0.5f, this.tween.Value);
        }

        [TestMethod]
        public void FinishedARepetitionMarksTweenAsFinished()
        {
            this.tween.Repeats = Repeat.Once;

            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(1, this.tween.Value);
            Assert.IsTrue(this.tween.IsFinished);
        }

        [TestMethod]
        public void AYoYoTweenReversedOnFinish()
        {
            this.tween.Repeats = Repeat.Once;
            this.tween.YoYos = true;

            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.tween.Value);
            Assert.IsTrue(this.tween.IsFinished);
        }

        [TestMethod]
        public void AYoYoTweenWhichDoesNotRepeatEndsImmediately()
        {
            this.tween.YoYos = true;

            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(1, this.tween.Value);
            Assert.IsTrue(this.tween.IsFinished);
        }

        [TestMethod]
        public void RepeatingATweenResetsTheDelay()
        {
            this.tween.Repeats = Repeat.Once;
            this.tween.PreDelay = TimeSpan.FromSeconds(1);

            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.tween.Value);
        }

        [TestMethod]
        public void YoYoingDoesNotResetDelayOnReverse()
        {
            this.tween.Repeats = Repeat.Once;
            this.tween.YoYos = true;
            this.tween.PreDelay = TimeSpan.FromSeconds(1);

            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.tween.Value);
            Assert.IsTrue(this.tween.IsFinished);
        }

        [TestMethod]
        public void YoYoingDoesResetDelayOnRepeat()
        {
            this.tween.Repeats = Repeat.Twice;
            this.tween.YoYos = true;
            this.tween.PreDelay = TimeSpan.FromSeconds(1);

            this.tween.Update(TimeSpan.FromSeconds(1)); // delay
            this.tween.Update(TimeSpan.FromSeconds(1)); // first play through
            this.tween.Update(TimeSpan.FromSeconds(1)); // reverse play through
            this.tween.Update(TimeSpan.FromSeconds(1)); // second delay
            this.tween.Update(TimeSpan.FromSeconds(1)); // second play through

            Assert.AreEqual(1, this.tween.Value);
            Assert.IsTrue(this.tween.IsFinished);
        }

        [TestMethod]
        public void RepeatingDoesNotMarkTweenAsFinished()
        {
            this.tween.Repeats = Repeat.Once;

            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.IsFalse(this.tween.IsFinished);
        }

        [TestMethod]
        public void RepeatingDoesNotInvokeFinishedCallback()
        {
            this.tween.Repeats = Repeat.Once;
            int numberOfCalls = 0;
            this.tween.FinishCallback = f => numberOfCalls++;

            this.tween.Update(TimeSpan.FromSeconds(1));
            this.tween.Update(TimeSpan.FromSeconds(1));

            Assert.AreEqual(1, numberOfCalls);
        }
    }
}