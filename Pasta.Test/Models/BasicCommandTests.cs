﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models;

namespace Pasta.Test.Models
{
    [TestClass]
    public class BasicCommandTests
    {
        private int callbackInvocations;
        private bool canExecuteResult;

        [TestInitialize]
        public void TestInitialize()
        {
            this.callbackInvocations = 0;
        }

        [TestMethod]
        public void WhenCanExecuteHandlerIsNullCommandCanBeExecuted()
        {
            this.canExecuteResult = false;
            var command = new BasicCommand(this.CallbackHandler, null);

            Assert.IsTrue(command.CanExecute(null));
        }

        [TestMethod]
        public void WhenCanExecuteIsFalseCommandCannotBeExecuted()
        {
            this.canExecuteResult = false;
            var command = new BasicCommand(this.CallbackHandler, this.CanExecuteHandler);

            Assert.IsFalse(command.CanExecute(null));
        }

        [TestMethod]
        public void WhenCanExecuteIsTrueCommandCannotBeExecuted()
        {
            this.canExecuteResult = true;
            var command = new BasicCommand(this.CallbackHandler, this.CanExecuteHandler);

            Assert.IsTrue(command.CanExecute(null));
        }

        [TestMethod]
        public void ExecutingCommandWhenCanExecuteIsFalseDoesNothing()
        {
            this.canExecuteResult = false;
            var command = new BasicCommand(this.CallbackHandler, this.CanExecuteHandler);

            command.Execute(null);

            Assert.AreEqual(0, this.callbackInvocations);
        }

        [TestMethod]
        public void ExecutingCommandInvokesCallback()
        {
            this.canExecuteResult = true;
            var command = new BasicCommand(this.CallbackHandler, this.CanExecuteHandler);

            command.Execute(null);

            Assert.AreEqual(1, this.callbackInvocations);
        }

        private bool CanExecuteHandler()
        {
            return this.canExecuteResult;
        }

        private void CallbackHandler()
        {
            this.callbackInvocations++;
        }
    }
}