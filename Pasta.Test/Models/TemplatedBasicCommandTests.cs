﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models;

namespace Pasta.Test.Models
{
    [TestClass]
    public class TemplatedBasicCommandTests
    {
        private int callbackInvocations;
        private bool canExecuteResult;
        private string callbackParameter;

        [TestInitialize]
        public void TestInitialize()
        {
            this.callbackInvocations = 0;
            this.callbackParameter = null;
        }

        [TestMethod]
        public void WhenCanExecuteHandlerIsNullCommandCanBeExecuted()
        {
            this.canExecuteResult = false;
            var command = new BasicCommand<string>(this.CallbackHandler, null);

            Assert.IsTrue(command.CanExecute("foobar"));
        }

        [TestMethod]
        public void WhenCanExecuteIsFalseCommandCannotBeExecuted()
        {
            this.canExecuteResult = false;
            var command = new BasicCommand<string>(this.CallbackHandler, this.CanExecuteHandler);

            Assert.IsFalse(command.CanExecute("foobar"));
        }

        [TestMethod]
        public void WhenCanExecuteIsTrueCommandCannotBeExecuted()
        {
            this.canExecuteResult = true;
            var command = new BasicCommand<string>(this.CallbackHandler, this.CanExecuteHandler);

            Assert.IsTrue(command.CanExecute("foobar"));
        }

        [TestMethod]
        public void ExecutingCommandWhenCanExecuteIsFalseDoesNothing()
        {
            this.canExecuteResult = false;
            var command = new BasicCommand<string>(this.CallbackHandler, this.CanExecuteHandler);

            command.Execute("foobar");

            Assert.AreEqual(0, this.callbackInvocations);
        }

        [TestMethod]
        public void ExecutingCommandInvokesCallback()
        {
            this.canExecuteResult = true;
            var command = new BasicCommand<string>(this.CallbackHandler, this.CanExecuteHandler);

            command.Execute("foobar");

            Assert.AreEqual(1, this.callbackInvocations);
            Assert.AreEqual("foobar", this.callbackParameter);
        }

        [TestMethod]
        public void ExecutingCommandWithInvalidParameterThrowsException()
        {
            this.canExecuteResult = true;
            var command = new BasicCommand<string>(this.CallbackHandler, this.CanExecuteHandler);

            try
            {
                command.Execute(1234);
                Assert.Fail();
            }
            catch (InvalidCastException)
            {
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        private bool CanExecuteHandler(string parameter)
        {
            return this.canExecuteResult;
        }

        private void CallbackHandler(string parameter)
        {
            this.callbackParameter = parameter;
            this.callbackInvocations++;
        }
    }
}