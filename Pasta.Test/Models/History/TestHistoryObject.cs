﻿using Pasta.Models.History;

namespace Pasta.Test.Models.History
{
    public class TestHistoryObject : IHistoryObject<string>
    {
        public string Value { get; set; }

        public int ResetInvocations { get; private set; }

        public void FromData(string data)
        {
            this.Value = data;
        }

        public string ToData()
        {
            return this.Value;
        }

        public void Reset()
        {
            this.ResetInvocations++;
            this.Value = null;
        }
    }
}