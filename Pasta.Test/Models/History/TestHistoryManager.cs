﻿using Pasta.Models.History;
using Pasta.Models.Streams;

namespace Pasta.Test.Models.History
{
    public class TestHistoryManager : HistoryManagerBase<string>
    {
        public TestHistoryManager(
            int version,
            IHistoryObject<string> historyObject,
            IStreamReader<string> reader,
            IStreamWriter<string> writer, 
            params IHistoryUpgrader[] upgraders)
            : base(version, historyObject, reader, writer, upgraders)
        {
        }

        public HistoryWrapper ReadWrapperOutput { get; set; }
        public int ReadWrapperInvocations { get; private set; }
        public int WriteWrapperInvocations { get; private set; }
        public HistoryWrapper WriteWrapperInput { get; private set; }
        public int ClearWrapperInvocations { get; private set; }

        protected override HistoryWrapper ReadWrapper()
        {
            this.ReadWrapperInvocations++;
            return this.ReadWrapperOutput;
        }

        protected override void WriteWrapper(HistoryWrapper wrapper)
        {
            this.WriteWrapperInvocations++;
            this.WriteWrapperInput = wrapper;
        }

        protected override void ClearWrapper()
        {
            this.ClearWrapperInvocations++;
        }
    }
}