﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models.History;
using Pasta.Models.Streams;

namespace Pasta.Test.Models.History
{
    [TestClass]
    public class HistoryManagerTests
    {
        private TestHistoryManager historyManager;
        private TestHistoryObject historyObject;
        private byte[] foobarBytes;
        private byte[] helloBytes;

        [TestInitialize]
        public void DoSomething()
        {
            this.historyObject = new TestHistoryObject();
            this.historyManager = new TestHistoryManager(
                2,
                this.historyObject,
                new JsonReader<string>(),
                new JsonWriter<string>());

            UTF8Encoding encoding = new UTF8Encoding();
            this.foobarBytes = encoding.GetBytes("\"Foobar\"");
            this.helloBytes = encoding.GetBytes("\"Hello World\"");
        }

        [TestMethod]
        public void ReadingHistoryInvokesReadWrapper()
        {
            this.historyManager.ReadHistory();

            Assert.AreEqual(1, this.historyManager.ReadWrapperInvocations);
        }

        [TestMethod]
        public void ReadingHistoryRestoresHistoryObject()
        {
            this.historyManager.ReadWrapperOutput = new HistoryWrapper()
                                                        {
                                                            Version = 2,
                                                            DateCreated = DateTime.UtcNow,
                                                            Data = this.foobarBytes
                                                        };

            this.historyManager.ReadHistory();

            Assert.AreEqual("Foobar", this.historyObject.Value);
        }

        [TestMethod]
        public void ReadingAnOlderWrapperDoesNothing()
        {
            var firstWrapper = new HistoryWrapper()
                              {
                                  Version = 2,
                                  DateCreated = DateTime.UtcNow,
                                  Data = this.foobarBytes
                              };

            this.historyManager.ReadWrapperOutput = firstWrapper;
            this.historyManager.ReadHistory();

            var secondWrapper = new HistoryWrapper()
            {
                Version = 2,
                DateCreated = DateTime.UtcNow.AddDays(-1),
                Data = this.helloBytes
            };
            this.historyManager.ReadWrapperOutput = secondWrapper;
            this.historyManager.ReadHistory();

            Assert.AreEqual(2, this.historyManager.ReadWrapperInvocations);
            Assert.AreEqual("Foobar", this.historyObject.Value);
        }

        [TestMethod]
        public void WritingInvokesWriteWrapper()
        {
            this.historyManager.WriteHistory();

            Assert.AreEqual(1, this.historyManager.WriteWrapperInvocations);
        }

        [TestMethod]
        public void WritingSetsWrapperProperties()
        {
            DateTime then = DateTime.UtcNow;
            this.historyObject.Value = "Foobar";

            this.historyManager.WriteHistory();

            Assert.AreEqual(2, this.historyManager.WriteWrapperInput.Version);
            Assert.IsTrue(then <= this.historyManager.WriteWrapperInput.DateCreated);
            Assert.AreEqual(this.foobarBytes.Length, this.historyManager.WriteWrapperInput.Data.Length);
        }

        [TestMethod]
        public void WritingHistoryThenReadingOldUpdateDoesNothing()
        {
            this.historyObject.Value = "Foobar";
            this.historyManager.WriteHistory();

            var update = new HistoryWrapper()
            {
                Version = 2,
                DateCreated = DateTime.UtcNow.AddDays(-1),
                Data = this.helloBytes
            };

            this.historyManager.ReadWrapperOutput = update;
            this.historyManager.ReadHistory();

            Assert.AreEqual(this.foobarBytes.Length, this.historyManager.WriteWrapperInput.Data.Length);
            Assert.AreEqual("Foobar", this.historyObject.Value);
        }

        [TestMethod]
        public void ClearingHistoryResetsObject()
        {
            this.historyManager.ClearHistory();

            Assert.AreEqual(1, this.historyObject.ResetInvocations);
        }

        [TestMethod]
        public void ClearingHistoryClearsWrapper()
        {
            this.historyManager.ClearHistory();

            Assert.AreEqual(1, this.historyManager.ClearWrapperInvocations);
        }
    }
}