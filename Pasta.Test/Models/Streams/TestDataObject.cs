﻿using System.Runtime.Serialization;

namespace Pasta.Test.Models.Streams
{
    [DataContract]
    public class TestDataObject
    {
        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 1)]
        public int Id { get; set; }
    }
}