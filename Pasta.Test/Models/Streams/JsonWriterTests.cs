﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models.Streams;

namespace Pasta.Test.Models.Streams
{
    [TestClass]
    public class JsonWriterTests
    {
        private JsonWriter<TestDataObject> writer;
        private TestDataObject testDataObject;
        private byte[] validJsonBytes;

        [TestInitialize]
        public void TestInitialize()
        {
            UTF8Encoding encoding = new UTF8Encoding();
            this.validJsonBytes = encoding.GetBytes("{\"Name\":\"Foobar\",\"Id\":1234}");
            this.testDataObject = new TestDataObject() { Name = "Foobar", Id = 1234 };
            this.writer = new JsonWriter<TestDataObject>();
        }

        [TestMethod]
        public void WritingObjectProducesJson()
        {
            var bytes = this.writer.Write(this.testDataObject);
            if (this.validJsonBytes.Length != bytes.Length)
            {
                Assert.Fail("Byte arrays vary by length");
            }

            for (int i = 0; i < this.validJsonBytes.Length; i++)
            {
                if (this.validJsonBytes[i] != bytes[i])
                {
                    Assert.Fail("Byte arrays vary by value");
                }
            }

            Assert.IsTrue(true);
        }
    }
}