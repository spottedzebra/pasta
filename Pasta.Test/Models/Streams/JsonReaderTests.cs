﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models.Streams;
using System.Runtime.Serialization;

namespace Pasta.Test.Models.Streams
{
    [TestClass]
    public class JsonReaderTests
    {
        private JsonReader<TestDataObject> reader;
        private byte[] validJsonBytes;
        private byte[] invalidJsonBytes;

        [TestInitialize]
        public void TestInitialize()
        {
            UTF8Encoding encoding = new UTF8Encoding();
            this.validJsonBytes = encoding.GetBytes("{\"Name\":\"Foobar\",\"Id\":1234}");
            this.invalidJsonBytes = encoding.GetBytes("{Name\":\"Foobar\",\"Id\":1234}");
            this.reader = new JsonReader<TestDataObject>();
        }

        [TestMethod]
        public void ReadingValidJsonReturnsObject()
        {
            TestDataObject result = this.reader.Read(this.validJsonBytes);

            Assert.AreEqual("Foobar", result.Name);
            Assert.AreEqual(1234, result.Id);
        }

        [TestMethod]
        public void ReadingInvalidJsonThrowsException()
        {
            try
            {
                TestDataObject result = this.reader.Read(this.invalidJsonBytes);
                Assert.Fail();
            }
            catch (SerializationException)
            {
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
    }
}