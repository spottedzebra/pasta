﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models.Licensing;

namespace Pasta.Test.Models.Licensing
{
    [TestClass]
    public class LicenseManagerTests
    {
        private TestLicenseManager licenseManager;

        [TestInitialize]
        public void TestInitialize()
        {
            this.licenseManager = new TestLicenseManager();
        }

        [TestMethod]
        public void ANewManagerDoesNotReadLicense()
        {
            Assert.AreEqual(0, this.licenseManager.ReadLicenseInvocations);
        }

        [TestMethod]
        public void InitializingReadsLicense()
        {
            this.licenseManager.Initialize();

            Assert.AreEqual(1, this.licenseManager.ReadLicenseInvocations);
        }

        [TestMethod]
        public void AnExpiredLicenseIsExpired()
        {
            this.licenseManager.HasExpiredOutput = true;
            this.licenseManager.ReadLicenseOutput = new License();

            this.licenseManager.Initialize();
            bool isExpired = this.licenseManager.IsLicenseExpired;

            Assert.AreEqual(1, this.licenseManager.HasExpiredInvocations);
            Assert.IsTrue(isExpired);
        }

        [TestMethod]
        public void ANewTrialIsNotExpired()
        {
            this.licenseManager.HasExpiredOutput = false;
            this.licenseManager.ReadLicenseOutput = new License();

            this.licenseManager.Initialize();
            bool isExpired = this.licenseManager.IsLicenseExpired;

            Assert.AreEqual(1, this.licenseManager.HasExpiredInvocations);
            Assert.IsFalse(isExpired);
        }

        [TestMethod]
        public void ALicensedLicenseIsNotExpired()
        {
            this.licenseManager.HasExpiredOutput = true;
            this.licenseManager.ReadLicenseOutput = new License() { IsLicensed = true };

            this.licenseManager.Initialize();
            bool isExpired = this.licenseManager.IsLicenseExpired;

            Assert.AreEqual(0, this.licenseManager.HasExpiredInvocations);
            Assert.IsFalse(isExpired);
        }

        [TestMethod]
        public void CheckingForExpirationForValidLicenseDoesNothing()
        {
            this.licenseManager.HasExpiredOutput = false;
            this.licenseManager.ReadLicenseOutput = new License();

            this.licenseManager.Initialize();
            this.licenseManager.CheckForExpiration();

            Assert.AreEqual(1, this.licenseManager.HasExpiredInvocations);
        }

        [TestMethod]
        public void CheckingForExpirationForInvalidLicenseRaisesEvent()
        {
            bool licenseExpiredEventFired = false;
            this.licenseManager.LicenseExpired += (sender, args) => licenseExpiredEventFired = true;

            this.licenseManager.HasExpiredOutput = true;
            this.licenseManager.ReadLicenseOutput = new License();

            this.licenseManager.Initialize();
            this.licenseManager.CheckForExpiration();

            Assert.IsTrue(licenseExpiredEventFired);
        }

        [TestMethod]
        public void PurchasingGameInvokesOnPurchaseGame()
        {
            this.licenseManager.Initialize();

            this.licenseManager.PurchaseGame();

            Assert.AreEqual(1, this.licenseManager.OnPurchaseGameInvocations);
        }

        [TestMethod]
        public void PurchasingGameFiresEvent()
        {
            bool gamePurchasedEventFired = false;
            this.licenseManager.GamePurchased += (sender, args) => gamePurchasedEventFired = true;

            this.licenseManager.Initialize();
            this.licenseManager.PurchaseGame();

            Assert.IsTrue(gamePurchasedEventFired);
        }
    }
}