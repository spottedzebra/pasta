﻿using Pasta.Models.Licensing;

namespace Pasta.Test.Models.Licensing
{
    public class TestLicenseManager : LicenseManagerBase
    {
        public TestLicenseManager() 
            : base(null, null)
        {
        }

        public License ReadLicenseOutput { get; set; }
        public int ReadLicenseInvocations { get; private set; }
        public int HasExpiredInvocations { get; private set; }
        public bool HasExpiredOutput { get; set; }
        public int OnPurchaseGameInvocations { get; private set; }

        protected override License ReadLicense()
        {
            this.ReadLicenseInvocations++;
            return this.ReadLicenseOutput;
        }

        protected override bool HasExpired(License license)
        {
            this.HasExpiredInvocations++;
            return this.HasExpiredOutput;
        }

        protected override void OnPurchaseGame(License license)
        {
            this.OnPurchaseGameInvocations++;
            this.RaiseGamePurchased();
        }
    }
}