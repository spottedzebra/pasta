﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pasta.Models.Collections;

namespace Pasta.Test.Models.Collections
{
    [TestClass]
    public class ObservableBagTests
    {
        private ObservableBag<int> bag;

        [TestInitialize]
        public void TestInitalize()
        {
            this.bag = new ObservableBag<int>();
        }

        [TestMethod]
        public void AddingAnItemFiresEvent()
        {
            int addedItem = -1;
            bool addedEventFired = false;
            this.bag.ItemAdded += (sender, args) =>
                                      {
                                          addedEventFired = true;
                                          addedItem = args.Item;
                                      };

            this.bag.Add(10);

            Assert.IsTrue(addedEventFired);
            Assert.AreEqual(10, addedItem);
            Assert.AreEqual(1, this.bag.Count);
        }

        [TestMethod]
        public void RemovingAnItemFiresEvent()
        {
            this.bag.Add(10);
            int removedItem = -1;
            bool removedEventFired = false;
            this.bag.ItemRemoved += (sender, args) =>
            {
                removedEventFired = true;
                removedItem = args.Item;
            };

            this.bag.Remove(10);

            Assert.IsTrue(removedEventFired);
            Assert.AreEqual(10, removedItem);
            Assert.AreEqual(0, this.bag.Count);
        }

        [TestMethod]
        public void RemovingAnItemByIndexFiresEvent()
        {
            this.bag.Add(10);
            int removedItem = -1;
            bool removedEventFired = false;
            this.bag.ItemRemoved += (sender, args) =>
            {
                removedEventFired = true;
                removedItem = args.Item;
            };

            this.bag.RemoveAt(0);

            Assert.IsTrue(removedEventFired);
            Assert.AreEqual(10, removedItem);
            Assert.AreEqual(0, this.bag.Count);
        }

        [TestMethod]
        public void CLearingFiresEvent()
        {
            bool clearedEventFires = false;
            this.bag.ItemsCleared += (sender, args) => clearedEventFires = true;

            this.bag.Add(1);
            this.bag.Clear();

            Assert.IsTrue(clearedEventFires);
            Assert.AreEqual(0, this.bag.Count);
        }
    }
}